<!-- View for Items wihout a group -->
<div class="group-card">
<div class="group-card-heading">
  Items zonder groep
</div>
<div class="group-card-content">
<ul class="group-card-list">
  <?php foreach($items->getItemsWithoutGroup() as $item): ?>
      <a href="/item.php?itemId=<?php echo htmlspecialchars($item["id"], ENT_QUOTES, 'UTF-8'); ?>">
      <li class="group-card-list-item">
        <?php echo htmlspecialchars($item["name"], ENT_QUOTES, 'UTF-8'); ?>
      </li>
    </a>
  <?php endforeach; ?>
</ul>
</div>
</div>
