<?php

/* Model Group, used for getting data from Database */
class Group {
  private $db;

 /* Singleton */
  public function __construct(){
    $this->db = DBConnection::getInstance();
  }

  /* Gets all groups from database */
  public function getAll(){
   $sql= "SELECT * FROM Groups";
   $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
   $query->execute();
   $groups = $query->fetchAll();
   return $groups;
  }

  /* Insert group into database optional with another attached group */
  public function insert($name, $group){
    $query = $this->db->prepare("INSERT INTO Groups (name,groups) VALUES(:name,:groups)");
    if($group == "") {$group = null;}
    $query->bindParam(':name', $name);
    $query->bindParam(':groups',$group);
    $query->execute();
    header("Location: index.php");
    die();
  }

}

 ?>
