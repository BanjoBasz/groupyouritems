<?php

/* Item model used for retrieve Item data from database */
class Item {
  private $db;

  /* Singleton */
  public function __construct(){
    $this->db = DBConnection::getInstance();
  }

  /* Gets all Items from database */
  public function getAll(){
   $sql= "SELECT * FROM Items";
   $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
   $query->execute();
   $items = $query->fetchAll();
   return $items;
  }

  /* Gets Item based who are in a group based on Group id*/
  public function getItems($id){
   $sql= "SELECT i.name, i.id FROM Items i INNER JOIN Groups g ON g.id = i.groups WHERE g.id = $id";
   $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
   $query->execute();
   $groups = $query->fetchAll();
   return $groups;
  }

  /* Gets Item based on Id */
  public function getItem($id){
   $sql= "SELECT name FROM Items where id = $id";
   $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
   $query->execute();
   $groups = $query->fetchAll();
   return $groups;
  }

  /* Gets all Items without a group */
  public function getItemsWithoutGroup(){
    $sql= "SELECT * FROM Items WHERE groups IS NULL";
    $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
    $query->execute();
    $groups = $query->fetchAll();
    return $groups;
  }

  /* Insert Item in database */
  public function insert($name, $group){
    $query = $this->db->prepare("INSERT INTO Items (name,groups) VALUES(:name,:groups)");
    if($group == "") {$group = null;}
    $query->bindParam(':name', $name);
    $query->bindParam(':groups',$group);
    $query->execute();
    header("Location: index.php");
    die();

  }


}

 ?>
