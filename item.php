<!-- Detail page for Item -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Item</title>
  <link rel="stylesheet" href="/css/master.css">
  <?php
  require_once("setup/config.php");
  require_once("models/Group.php");
  require_once("models/Item.php");
  ?>

</head>
  <?php
    $item = new Item();

    if(isset($_GET['itemId']) && $_GET["itemId"] != null) {
      $itemId = $_GET['itemId'];

    } else{
      //Sets Item ID to unreal value. Hack ;)
      $itemId = "999999999";
    }
    $result = $item->getItem($itemId);
  ?>
<body>
  <main>
    <div class="item-card">
      <div class="item-card-heading">
        <?php
          if($result[0]["name"] != "") {
            echo htmlspecialchars($result[0]["name"], ENT_QUOTES, 'UTF-8');
          }
          else {
            echo "Deze pagina is niet gevonden :(";
          }
        ?>
      </div>
    </div>
  </main>
</body>
</html>
