<!-- Main page, displays groups with Items -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="/css/master.css">
  <title>Home</title>
  <?php
  require_once("setup/config.php");
  require_once("models/Group.php");
  require_once("models/Item.php");
  ?>

</head>
<body>
  <main>
    <?php
      $group = new Group();
      $groups = $group->getAll();
      $items = new Item();

      /*
      $item = new Item();
    var_dump(  $item->getItems(10003));
      */
    ?>
    <?php foreach ($groups as $index => $group): ?>
          <div class="group-card ">
          <div class="group-card-heading">
            <?php echo htmlspecialchars($group["name"], ENT_QUOTES, 'UTF-8'); ?>
          </div>
          <div class="group-card-content">
          <ul class="group-card-list">
            <?php foreach($items->getItems($group["id"]) as $item): ?>
                <a href="/item.php?itemId=<?php echo htmlspecialchars($item["id"], ENT_QUOTES, 'UTF-8');?>">
                <li class="group-card-list-item">
                  <?php echo htmlspecialchars($item["name"], ENT_QUOTES, 'UTF-8');?>
                </li>
              </a>
            <?php endforeach; ?>
            <?php if($group["groups"] != "") : ?>
              <li class="group-card-list-item--group">
                Behoord ook tot de groep: <br>
                <?php echo htmlspecialchars($group["groups"], ENT_QUOTES, 'UTF-8'); ?>
              </li>
            <?php endif; ?>
          </ul>
          </div>
         </div>
    <?php endforeach; ?>

    <?php include('groupless.php') ?>
    <?php include('buttons.php') ?>

  </main>
</body>
</html>
