<?php

/* DBConnection class used for exec querys on database */
class DBConnection{
    protected static $db;

    /* Fill your own credentials in here!*/
    private function __construct() {
        try {
            self::$db = new PDO( 'mysql:host=localhost;dbname=phpTest', 'jeroen', 'NietMijnW@chtwoord' );
            self::$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        catch (PDOException $e) {
            echo "Connection Error: " . $e->getMessage();
        }
    }
    /*Singleton instance */
    public static function getInstance() {
        if (!self::$db) {
            new DBConnection();
        }
        return self::$db;
    }

    public function __destruct() {
        $db=NULL;
    }
}

?>
