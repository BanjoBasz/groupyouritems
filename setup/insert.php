<?php

/* Script used for demo purposes, insert rows in tables */
require("config.php");

$db = DBConnection::getInstance();

$insertGroup1 = "INSERT INTO Groups(id,name)
VALUES (10004,'Voertuigen')";

$insertGroup2 = "INSERT INTO Groups(id,name, groups)
VALUES (10003,'Auto', 10004)";

$insertItem1 = "INSERT INTO Items (name,groups)
VALUES ('Fiat Panda',10003)";

$insertItem2 = "INSERT INTO Items (name)
VALUES ('Japanese for busy people')";

$insertItem3 = "INSERT INTO Items (name,groups)
VALUES ('KLM Vliegtuig',10004)";

$insertItem4 = "INSERT INTO Items (name,groups)
VALUES ('Fiat Punto',10003)";


$db->query($insertGroup1);
$db->query($insertGroup2);
$db->query($insertItem1);
$db->query($insertItem2);
$db->query($insertItem3);
$db->query($insertItem4);


?>
