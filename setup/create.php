<?php

/* Script used for demo purposes, created tables */
require("config.php");

$db = DBConnection::getInstance();

// sql to create table
$sqlGroup = "CREATE TABLE Groups (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30),
  groups INT(6) UNSIGNED,
  FOREIGN KEY (groups) REFERENCES Groups (id)
)";

// sql to create table
$sqlItem = "CREATE TABLE Items (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) NOT NULL,
  groups INT(6) UNSIGNED,
  FOREIGN KEY (groups) REFERENCES Groups (id)
)";

$db->exec($sqlGroup);
$db->exec($sqlItem);

?>
