He! Welkom bij mijn Item en Groepen app. Om de app te runnen:

1. Set jou credentials in de config.php file (setup/config.php).
2. Run create.php (setup/config.php). Bij geen foutmeldingen zijn de tabellen aangemaakt.
3. Run insert.php (setup/insert.php). Bij geen foutmelding is er dummy data in de database gezet.

Verbeterpunten indien meer tijd:
- Responsiviteit
- Factory / Repository design pattern toepassen voor Items en Groupen
- Template language zoals Blade gebruiken voor het renderen van HTML
