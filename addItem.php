<!-- View for adding a Item to the database -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Item toevoegen</title>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="/css/master.css">
  <?php
  require_once("setup/config.php");
  require_once("models/Group.php");
  require_once("models/Item.php");

  $groupModel = new Group();
  $groups = $groupModel->getAll();

  ?>
</head>
<body>
  <main>
    <div class="add-card">
      <div class="add-card-heading">
        Voeg een Item toe
      </div>
      <div class="add-card-form">
        <form action="addItem.php" method="POST">
            <label for="name">Naam</label>
            <input class = "biggerInput" name="name" type="text"><br>
            <label for="group">Groep</label>
            <select name="group" class="biggerInput">
              <?php foreach($groups as $group): ?>
                <option value=<?php echo htmlspecialchars($group["id"], ENT_QUOTES, 'UTF-8'); ?>>
                  <?php echo htmlspecialchars($group["name"], ENT_QUOTES, 'UTF-8'); ?>
                </option>
              <?php endforeach; ?>
              <option value="">Geen</option>
            </select>
            <input class="btn" type="submit" value="Aanmaken" name="submit">
        </form>
      </div>
    </div>
    <?php
      $itemInsert = new Item();
      if(isset($_POST["submit"]) && $_POST["name"] != ""){
          $itemInsert->insert($_POST["name"],$_POST["group"]);
      }
    ?>
  </main>
</body>
</html>
